#!/usr/bin/env bash

CMD="clang-tidy"
if [ -n "${1}" ]
then
	if [ "${1}" -eq "${1}" ] 2>/dev/null
	then
		CMD="${CMD}-${1}"
	else
		CMD="${1}"
	fi
fi
if ! [ -x "$(command -v "${CMD}")" ]; then
  echo "Error: \"${CMD}\" is not a valid command" >&2
  exit 1
fi

VERSION=$("${CMD}" --version | sed -nE "s/^.* version ([0-9]+).*/\1/p")
if [ -z "${VERSION}" ]; then
	echo "Error: \"${CMD}\" is not a valid clang-tidy executable" >&2
	exit 2
fi

OUTPUT="$(pwd)/clang-tidy-${VERSION}_default.yml"
echo "Generating default configuration for clang-tidy-${VERSION} in \"${OUTPUT}\""

"${CMD}" --checks="*" --dump-config | yq eval-all 'sort_keys(..) | with(select(.CheckOptions | tag == "!!seq"); .CheckOptions |= sort_by(.key))' - > "${OUTPUT}"

