#!/usr/bin/env bash

IN="${1}"
OUT="${2}"

yq eval-all '.CheckOptions |= (((.[] | select (. != null)) as $i ireduce({}; .[$i.key] = $i.value))) | sort_keys(..)' "${IN}" > "${OUT}"

