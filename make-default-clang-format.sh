#!/usr/bin/env bash

CMD="clang-format"
if [ -n "${1}" ]
then
	if [ "${1}" -eq "${1}" ] 2>/dev/null
	then
		CMD="${CMD}-${1}"
	else
		CMD="${1}"
	fi
fi
if ! [ -x "$(command -v "${CMD}")" ]; then
  echo "Error: \"${CMD}\" is not a valid command" >&2
  exit 1
fi

VERSION=$("${CMD}" --version | sed -nE "s/^.* clang-format version ([0-9]+).*/\1/p")
if [ -z "${VERSION}" ]; then
	echo "Error: \"${CMD}\" is not a valid clang-format executable" >&2
	exit 2
fi

OUTPUT="$(pwd)/clang-format-${VERSION}_default.yml"
echo "Generating default configuration for clang-format-${VERSION} in \"${OUTPUT}\""

"${CMD}" -style=llvm -dump-config | yq eval-all 'sort_keys(..)' - > "${OUTPUT}"

